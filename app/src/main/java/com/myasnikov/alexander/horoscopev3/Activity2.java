package com.myasnikov.alexander.horoscopev3;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;


public  class Activity2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity2);



        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec tabSpec;

        tabSpec = tabHost.newTabSpec("tag1");
        tabSpec.setIndicator("Сегодня");
        tabSpec.setContent(R.id.tvTab1);
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setIndicator("Завтра");
        tabSpec.setContent(R.id.tvTab2);
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag3");
        tabSpec.setIndicator("Послезавтра");
        tabSpec.setContent(R.id.tvTab3);
        tabHost.addTab(tabSpec);

        tabHost.setCurrentTabByTag("tag1");



        TextView tvTab1 = (TextView) findViewById(R.id.tvTab1);
        TextView tvTab2 = (TextView) findViewById(R.id.tvTab2);
        TextView tvTab3 = (TextView) findViewById(R.id.tvTab3);

        String file1 = getIntent().getStringExtra("file1");
        String file2 = getIntent().getStringExtra("file2");
        String file3 = getIntent().getStringExtra("file3");

        tvTab1.setMovementMethod(new ScrollingMovementMethod());
        tvTab2.setMovementMethod(new ScrollingMovementMethod());
        tvTab3.setMovementMethod(new ScrollingMovementMethod());

        {
            try {
                InputStream is = getAssets().open(file1);
                int size = is.available();

                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                tvTab1.setText(new String(buffer));
            } catch (Exception e){
            }

            try {
                InputStream is = getAssets().open(file2);
                int size = is.available();

                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                tvTab2.setText(new String(buffer));
            } catch (Exception e){
            }

            try {
                InputStream is = getAssets().open(file3);
                int size = is.available();

                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                tvTab3.setText(new String(buffer));
            } catch (Exception e){
            }
        }



        // обработчик переключения вкладок
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                // Toast.makeText(getBaseContext(), "tabId = " + tabId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void load() {

    }
}
