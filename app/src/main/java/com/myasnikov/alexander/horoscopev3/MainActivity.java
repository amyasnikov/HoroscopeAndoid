package com.myasnikov.alexander.horoscopev3;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;
    private final String[] itemdesc;

    public CustomListAdapter(Activity context, String[] itemname, Integer[] imgid, String[] itemdesc) {
        super(context, R.layout.mylist, itemname);
        // TODO Auto-generated constructor stub

        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
        this.itemdesc = itemdesc;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView extratxt = (TextView) rowView.findViewById(R.id.textView1);

        txtTitle.setText(itemname[position]);
        imageView.setImageResource(imgid[position]);
        extratxt.setText(itemdesc[position]);
        return rowView;

    };
}

public class MainActivity extends Activity{

    ListView list;
    String[] itemname = {
            "Овен",
            "Телец",
            "Близнецы",
            "Рак",
            "Лев",
            "Дева",
            "Весы",
            "Скорпион",
            "Стрелец",
            "Козерог",
            "Водолей",
            "Рыбы"
    };

    Integer[] imgid ={
            R.mipmap.aries,
            R.mipmap.taurus,
            R.mipmap.gemini,
            R.mipmap.cancer,
            R.mipmap.leo,
            R.mipmap.virgo,
            R.mipmap.libra,
            R.mipmap.scorpio,
            R.mipmap.sagittarius,
            R.mipmap.capricorn,
            R.mipmap.aquarius,
            R.mipmap.pisces,
    };

    String[] itemdesc = {
            "21 марта - 20 апреля",
            "21 апреля - 20 мая",
            "21 мая - 21 июня",
            "22 июня - 22 июля",
            "23 июля - 23 августа",
            "24 августа - 23 сентября",
            "24 сентября - 23 октября",
            "24 октября - 22 ноября",
            "23 ноября - 21 декабря",
            "22 декабря - 20 января",
            "21 января - 18 февраля",
            "19 февраля - 20 марта"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomListAdapter adapter = new CustomListAdapter(this, itemname, imgid, itemdesc);
        list = (ListView)findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                // String Slecteditem = itemname[position];
                // Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();

                String file1;
                String file2;
                String file3;
                String sign = itemname[position].toLowerCase();

                {
                    Date date = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    int year = 2016;
                    int month = 1 + cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    file1 = String.format("horoscope-%s-%02d-%02d-%04d", sign, day, month, year);
                }

                {
                    Date date = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    cal.add(Calendar.DATE, 1);
                    int year = 2016;
                    int month = 1 + cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    file2 = String.format("horoscope-%s-%02d-%02d-%04d", sign, day, month, year);
                }

                {
                    Date date = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    cal.add(Calendar.DATE, 2);
                    int year = 2016;
                    int month = 1 + cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    file3 = String.format("horoscope-%s-%02d-%02d-%04d", sign, day, month, year);
                }

                Intent intent = new Intent(MainActivity.this, Activity2.class);
                intent.putExtra("file1", file1);
                intent.putExtra("file2", file2);
                intent.putExtra("file3", file3);

                startActivity(intent);

            }
        });
    }
}